﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ucprotectionplans.utility
{
    public class ConnectionHelper
    {

        public SqlConnection con;

        public ConnectionHelper()
        {
            this.con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ToString());
        }
    }
}