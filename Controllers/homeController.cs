﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ucprotectionplans.utility;

namespace ucprotectionplans.Controllers
{
    public class homeController : Controller
    {
        // GET: home
        public ActionResult Index(string firstname = "", string lastname = "", string phone = "", string email = "", int c = -2)
        {
            if (!Request.IsSecureConnection&&!Request.IsLocal)
            {
                string _url = "https://www.ucprotectionplans.com?v=1";
                if (!string.IsNullOrEmpty(firstname))
                {
                    _url = _url + "&firstname=" + firstname;
                }
                if (!string.IsNullOrEmpty(lastname))
                {
                    _url = _url + "&lastname=" + lastname;
                }
                if (!string.IsNullOrEmpty(phone))
                {
                    _url = _url + "&phone=" + phone;
                }
                if (!string.IsNullOrEmpty(email))
                {
                    _url = _url + "&email=" + email;
                }
                if (c != -2)
                {
                    _url = _url + "&c=" + c;
                }
                return Redirect(_url);
            }
            string useragent = Request.UserAgent.ToString();
            string biltylink = "https://www.ucesprotectionplan.com/default.aspx?rid=MEsteche";
            ViewBag.click = biltylink;
            Session["firstname"] = firstname;
            Session["lastname"] = lastname;
            Session["phone"] = phone;
            Session["email"] = email;
            Session["c"] = c; 
            return View();
        }

        public string Index2(string firstname = "", string lastname = "", string phone = "", string email = "", int c = -2)
        { 
            string useragent = Request.UserAgent.ToString();
            string biltylink = "https://www.ucesprotectionplan.com/default.aspx?rid=MEsteche";
            ViewBag.click = biltylink;
            Session["firstname"] = firstname;
            Session["lastname"] = lastname;
            Session["phone"] = phone;
            Session["email"] = email;
            Session["c"] = c;
            return insertpaymentlog2(firstname, lastname, email, phone, c);
           
        }


        public void SendnotifyAsync()
        {
            string firstname = Session["firstname"] == null ? "" : Session["firstname"].ToString();
            string lastname = Session["lastname"] == null ? "" : Session["lastname"].ToString();
            string phone = Session["phone"] == null ? "" : Session["phone"].ToString();
            string email = Session["email"] == null ? "" : Session["email"].ToString();
            string c = Session["c"] == null ? "" : Session["c"].ToString();
            string alerturl = "https://getultimateoffer.com/user/getalertmessage?fname=" + firstname + "&lname=" + lastname + "&phone=" + phone + "&email=" + email + "&c=" + c;
            if (!string.IsNullOrEmpty(firstname))
            {
                Get(alerturl);
            }
        }

        public void SendvisitAsync()
        {
            string firstname = Session["firstname"] == null ? "" : Session["firstname"].ToString();
            string lastname = Session["lastname"] == null ? "" : Session["lastname"].ToString();
            string phone = Session["phone"] == null ? "" : Session["phone"].ToString();
            string email = Session["email"] == null ? "" : Session["email"].ToString();
            string c = Session["c"] == null ? "" : Session["c"].ToString();
            int c1 = !string.IsNullOrEmpty(c) ? Convert.ToInt32(c) : 0;
            insertpaymentlog(firstname, lastname, email, phone, c1);
            if (!string.IsNullOrEmpty(firstname) && !string.IsNullOrEmpty(lastname) )
            {
                string alerturl = "https://getultimateoffer.com/user/addalertmessage?fname=" + firstname + "&lname=" + lastname + "&phone=" + phone + "&email=" + email + "&c=" + c + "&url=";
                Get(alerturl);
            }
        }
        public string Get(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
        public void insertpaymentlog(string fname,string lname,string email,string phone,int c)
        {
            SqlHelper _dt = new SqlHelper(); 
            var ip = Request.UserHostAddress;
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@fname", fname);
                _srtlist.Add("@lname", lname);
                _srtlist.Add("@ip", ip); 
                _srtlist.Add("@email", email);
                _srtlist.Add("@phone", phone);
                _srtlist.Add("@c", c); 
                _dt.executeNonQuery("adducprotectionplanslog", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }
        public string insertpaymentlog2(string fname, string lname, string email, string phone, int c)
        {
            string message = "";
            SqlHelper _dt = new SqlHelper();
            var ip = Request.UserHostAddress;
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@fname", fname);
                _srtlist.Add("@lname", lname);
                _srtlist.Add("@ip", ip);
                _srtlist.Add("@email", email);
                _srtlist.Add("@phone", phone);
                _srtlist.Add("@c", c);
                _dt.executeNonQuery("adducprotectionplanslog", "", _srtlist);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
            return message;
        }

    }
}